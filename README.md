# graphql-schema-coursera

## Installation

    git clone https://bitbucket.org/mswinson-lib/graphql-schema-coursera  

## Prequisites

    docker
    docker-compose

## Usage

setup  

    export S3_BUCKET_REPO=<mybucket>
    export S3_BUCKET_PREFIX=<mybucket_prefix>
    export AWS_DEFAULT_REGION=<aws_region>

build  

    docker-compose run build

    > make build


deploy

    docker-compose run build

    > make deploy
  

## Contributing

