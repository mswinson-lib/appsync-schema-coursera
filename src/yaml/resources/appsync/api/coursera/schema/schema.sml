type Course {
  id: ID
  name: String
  courseType: String
  slug: String
}

type Catalog {
  courses: [ Course ]
}

type Api {
  catalog: Catalog
}

type Query {
  coursera: Api
}

schema {
  query: Query
}
